[ $DEBUG_DOTFILES ] && echo "Loading .zshrc"

# Load initialization
[ $DEBUG_DOTFILES ] && echo "  Loading ~/.config/shell/init"
source ~/.config/shell/init

# load the sourcesfile, which in turn will source more functionality
[ $DEBUG_DOTFILES ] && echo "  Loading ~/.config/shell/sources"
source ~/.config/shell/sources

# autoload compontents {{{
[ $DEBUG_DOTFILES ] && echo "  Autoloading components"
autoload -U colors && colors
autoload -U compinit && compinit
autoload -U promptinit && promptinit
# }}}

# set base options {{{
[ $DEBUG_DOTFILES ] && echo "  Setting base options"
setopt appendhistory autocd autopushd extendedglob histignorespace prompt_subst vi
unsetopt beep nomatch notify
# }}}

# vi mode
[ $DEBUG_DOTFILES ] && echo "  Setting vi-mode"
bindkey -v

# Extend vi mode with some happy good vim stuff
function zle-move-to-buffer-beginning {
	CURSOR=0
}

zle -N zle-move-to-buffer-beginning

# Bind some emacs-style bindings in insert mode
bindkey -M viins '^a' beginning-of-line
bindkey -M viins '^b' backward-char
bindkey -M viins '^e' end-of-line
bindkey -M viins '^f' forward-char
bindkey -M viins '^k' kill-line
bindkey -M viins '^n' history-beginning-search-forward
bindkey -M viins '^p' history-beginning-search-backward
bindkey -M viins '^u' kill-region
bindkey -M viins '^y' yank

# Bind the vim bindings
bindkey -M vicmd "gg" zle-move-to-buffer-beginning

# configure components {{{
[ $DEBUG_DOTFILES ] && echo "  Setting autocompletions"
zstyle :compinstall filename '/home/tyil/.zshrc'
zstyle ':completion:*' menu select
# }}}

function precmd() # {{{
{
	# format the last command's exitcode
	EXITCODE=$(printf "%03i" "$?")

	# set window title
	echo -en "\e]2;`whoami`@`hostname`:`pwd`\a"

	# git info {{{
	if git rev-parse --is-inside-work-tree &> /dev/null
	then
		# Set coloring of the branch name
		if [ "$(git status -uno --porcelain 2> /dev/null)" = "" ]
		then
			GITBRANCHC="%F{2}"
		else
			GITBRANCHC="%F{11}"
		fi

		# Get the branch name
		GITBRANCH=$(git rev-parse --abbrev-ref @)

		if [ "$GITBRANCH" = "HEAD" ]
		then
			GITBRANCH=$(git rev-parse --short HEAD 2> /dev/null)
		fi

		# Set the prompt
		PGIT="%F{8}·%f${GITBRANCHC}${GITBRANCH}%f %F{8}"
	else
		# Not in a git repo, set a regular prompt
		PGIT="%F{8}"
	fi
	# }}}
}
# }}}

# setup vi-mode indicators {{{
VIMODE_I="%F{13}"
VIMODE_N="%F{11}"
VIMODE_U="%F{2}"
# }}}

# set vimode coloring {{{
local vi_mode="$VIMODE_I"
vi_mode_indicator () {
	case ${KEYMAP} in
		(vicmd)      echo $VIMODE_N ;;
		(main|viins) echo $VIMODE_I ;;
		(*)          echo $VIMODE_U ;;
	esac
}
# }}}

# reset mode-marker and prompt whenever the keymap changes {{{
function zle-line-init zle-keymap-select {
	vi_mode="$(vi_mode_indicator)"
	zle reset-prompt
}

zle -N zle-line-init
zle -N zle-keymap-select
# }}}

# set the final prompt
[ $DEBUG_DOTFILES ] && echo "  Setting PS1"
PROMPT='%(!.%F{1}.%F{2})%n%F{8}'
PROMPT=$PROMPT'@'
PROMPT=$PROMPT'${vi_mode}%M%F{8}'
PROMPT=$PROMPT':'
PROMPT=$PROMPT'%F{12}%~%k${PGIT}'
PROMPT=$PROMPT' '
PROMPT=$PROMPT'%(?.%F{15}.%F{1})${EXITCODE}%k'
PROMPT=$PROMPT' %F{8}» %f${CURSOR_STYLE}'

[ $DEBUG_DOTFILES ] && echo "  Enable gpg-agent"
gpg-connect-agent /bye
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)

# Apply bundles
source "${XDG_CONFIG_DIR:-"${HOME}/.config"}/shell/vendor/zsh/feature-syntax-highlighting/zsh-syntax-highlighting.zsh"

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/bin/terraform terraform
