local capi = {
	mouse = mouse,
	screen = screen,
}
local naughty = require("naughty")

------------------------------------------
-- todo.txt popup widget
------------------------------------------

local todo = {}

function todo:new(args)
	return setmetatable({}, {__index = self})
end

function todo:show()
	local f = assert(io.open("/home/tyil/documents/todo.txt", "rb"))
	local content = f:read("*all"):gsub("%s+$", "")
	f:close()

	self.notification = naughty.notify({
		title = "Todo",
		text = content,
		timeout = 0,
		hover_timeout = 0.5,
		screen = capi.mouse.screen,
		position = self.position,
	})
end

function todo:hide()
	if self.notification then
		naughty.destroy(self.notification)
		self.notification = nil
		self.num_lines = 0
	end
end

function todo:attach(widget)
	widget:connect_signal('mouse::enter', function() self:show() end)
	widget:connect_signal('mouse::leave', function() self:hide() end)
end

return setmetatable(todo, {
	__call = todo.new,
})
